var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var ShoppingSubCategorySchema = new mongoose.Schema(
  {
      productId:Number,
    productName:String,
    subCategoryid:Number,
    categoryId: Number,
    productPrice:Number,
    productOfferid:Number,
    productAvailableqty:Number,
    productSoldqty:Number,
    productImage:Array,
    productShortdescription:String,
    productLongdescription:String

  },{collection: "Product"});
  mongoose.model('Product',ShoppingSubCategorySchema);
